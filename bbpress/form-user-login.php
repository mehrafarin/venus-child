<?php

/**
 * User Login Form
 *
 * @package bbPress
 * @subpackage Theme
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

global $wp;
$current_url = home_url( add_query_arg( array(), $wp->request ) );

?>
<div class="d-inline p-2">
  <a class="bsp_button1" href="<?php echo site_url( $path = '/login?redirect_to='.$current_url);?>">ورود به سایت</a>
</div>
<div class="d-inline p-2">
  <a class="bsp_button1" target="_blank" href="<?php echo site_url( $path = '/help/forums');?>">راهنما</a>
</div>
