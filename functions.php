<?php

/* *---------------------------------------------------------------------------
 * Venus - Child Theme Functions and Definitions
 * ----------------------------------------------------------------------------
 */

 function venus_child_enqueue_styles() {

    $parent_style = 'Kiwi';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'venus-child',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'venus_child_enqueue_styles' );



#----------------- Developers -----------------#
// COMBINE THE CART AND CHECKOUT PAGES
add_action( 'woocommerce_before_checkout_form', function () {
  if ( is_wc_endpoint_url( 'order-received' ) ) return;
      echo do_shortcode('[woocommerce_cart]');
}, 5 );

function takexpert_fix_unicode_numbers($string) {
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}

// convert gravityforms phone field persian/arabic to english
// add_filter("gform_save_field_value", "takexpert_gf_validate_phone", 10, 4);
// function takexpert_gf_validate_phone( $value, $entry, $field, $form ){
//   if($field->get_input_type() == "phone"){
//     $value = takexpert_fix_unicode_numbers($value);
//     if($value[0] = 0){
//       $value = ltrim($value, '0');
//     }
//     error_log("phone: ".$value, 0);
//   }
//   return $value;
// }

// populate user email field by fake email if empty after registeration
add_action('user_register','takexpert_user_registeration_fake_email');
function takexpert_user_registeration_fake_email($user_id){
  $user_data = get_userdata($user_id);
  if(empty($user_data->user_email)){
    wp_update_user(array('ID' => $user_id, 'user_email' => $user_data->user_login.'@'.parse_url(get_site_url(), PHP_URL_HOST)));
  }
  return $user_id;
}

//  populate woocommerce billing info by user info
// note: phone field populate with persian user panel field
add_filter('woocommerce_checkout_get_value', function($input, $key ) {
    global $current_user;
    switch ($key) :
        case 'billing_first_name':
        case 'shipping_first_name':
            return $current_user->first_name;
        break;

        case 'billing_last_name':
        case 'shipping_last_name':
            return $current_user->last_name;
        break;
        case 'billing_email':
            return $current_user->user_email;
        break;
        case 'billing_phone':
          if(!empty($current_user->wupp_mobile)){
            return $current_user->wupp_mobile;
          } else {
            return $current_user->user_login;
          }
        break;
    endswitch;
}, 10, 2);

function populate_woocommerce_billing_contact($order_id){
  // fix billing email if not exist
  $order = wc_get_order( $order_id );
  $order_data = $order->get_data(); // The Order data
  $order_billing_email = $order_data['billing']['email'];
  $order_billing_phone = $order_data['billing']['phone'];
  $user      = $order->get_user(); // Get the WP_User object
  // set billing email if empty
  if(empty($order_billing_email)){
    // set user email if empty
    if(empty($user->user_email)){
      takexpert_user_registeration_fake_email($user->ID);
    }
    $order_billing_email = $user->user_email;
  }
  // set billing phone if empty
  if(empty($order_billing_phone)){
    if(!empty($user->wupp_mobile)){
      $order_billing_phone = $user->wupp_mobile;
    } else {
      if(preg_match('/^[0-9]{9,14}\z/', $user->user_login)){
        $order_billing_phone = $user->user_login;
      } else {
        echo "error while set billing phone for user: ".
        $user->first_name.' '.$user->last_name;
      }
    }
  }
  $order = wc_update_order( array (
    'order_id' => $order_id,
    'email' => $order_billing_email,
    'phone' => $order_billing_phone
   ) );
}

/**
 * Regenerate the WooCommerce download permissions for an order
 * @param  Integer $order_id
 */
function regen_woo_downloadable_product_permissions( $product_id ){
  // get all WooCommerce complete orders id by product id
  global $wpdb;

  // Define HERE the orders status to include in  <==  <==  <==  <==  <==  <==  <==
  $orders_statuses = "'wc-completed'";

  # Get All defined statuses Orders IDs for a defined product ID (or variation ID)
  $orders_id = $wpdb->get_col( "
      SELECT DISTINCT woi.order_id
      FROM {$wpdb->prefix}woocommerce_order_itemmeta as woim,
           {$wpdb->prefix}woocommerce_order_items as woi,
           {$wpdb->prefix}posts as p
      WHERE  woi.order_item_id = woim.order_item_id
      AND woi.order_id = p.ID
      AND p.post_status IN ( $orders_statuses )
      AND woim.meta_key IN ( '_product_id', '_variation_id' )
      AND woim.meta_value LIKE '$product_id'
      ORDER BY woi.order_item_id DESC"
  );


    foreach ($orders_id as $order_id) {
      // populate order billing email and phone
      populate_woocommerce_billing_contact($order_id);

      // Remove all existing download permissions for this order.
      // This uses the same code as the "regenerate download permissions" action in the WP admin (https://github.com/woocommerce/woocommerce/blob/3.5.2/includes/admin/meta-boxes/class-wc-meta-box-order-actions.php#L129-L131)
      // An instance of the download's Data Store (WC_Customer_Download_Data_Store) is created and
      // uses its method to delete a download permission from the database by order ID.
      $data_store = WC_Data_Store::load( 'customer-download' );
      $data_store->delete_by_order_id( $order_id );

      // Run WooCommerce's built in function to create the permissions for an order (https://docs.woocommerce.com/wc-apidocs/function-wc_downloadable_product_permissions.html)
      // Setting the second "force" argument to true makes sure that this ignores the fact that permissions
      // have already been generated on the order.
      wc_downloadable_product_permissions( $order_id, true );
    }
}


// filter elementor countdown for meeting post type
add_action( 'init', function(){
    add_action( 'elementor/frontend/widget/before_render', function ( \Elementor\Element_Base $element ) {
        if ( "countdown"  != $element->get_name() || get_post_type( get_the_ID() ) != 'meeting') {
            return;
        }

        //Get the field specified on options page
        $countdown_date = get_post_meta(get_the_ID(), 'start_meeting', TRUE);
        //Do the thing
        $element->set_settings( 'due_date', date('Y-m-d H:i',strtotime($countdown_date))) ;
    }
);
});

// remove woocommerce remaining and expires columns
function filter_woocommerce_account_downloads_columns( $array ) {
    return array(
      'download-product' => __( 'Product', 'woocommerce' ),
      'download-file' => __( 'File', 'woocommerce' ),
      'download-actions' => ' ',
    );
};
add_filter( 'woocommerce_account_downloads_columns', 'filter_woocommerce_account_downloads_columns', 10, 1 );

// shortcode for get bbpress parent forum id
add_shortcode('get_grandpa_forum', 'get_grandpa_forum');
function get_grandpa_forum($atts){
  return $atts['id']->post_parent();
}

// auto redirect login page to previous page
add_action('login_auto_redirect', 'login_auto_redirect_hook');
function login_auto_redirect_hook(){
  ?>
  <a href="
   <?php echo esc_url(site_url('/login').'/?redirect_to='.get_permalink(get_the_id())); ?>"
   class="btn btn-primary user">ورود | عضویت</a>
  <?php
}

add_shortcode('login_auto_redirect', 'login_auto_redirect_shortcode');
function login_auto_redirect_shortcode(){
  return esc_url(site_url('/login').'/?redirect_to='.get_permalink(get_the_id()));
}

// check user exist (user panel pro plugin)
add_action('wp_ajax_takexpert_wupp_exist_user', 'takexpert_wupp_exist_user');
add_action('wp_ajax_nopriv_takexpert_wupp_exist_user', 'takexpert_wupp_exist_user');
function takexpert_wupp_exist_user() {
  $user_exist = 0;
	$user_login = $_POST['user_login'];
  if(preg_match('/09[0-9]{9}/', $user_login) && get_user_by('login', substr($user_login, 1))){
     $user_exist = 1;
  } else if(filter_var($user_login, FILTER_VALIDATE_EMAIL) && get_user_by('email', $user_login)){
    $user_exist = 1;
  }
  $user_exist ? wp_send_json_success():wp_send_json_error();
  wp_die();
}

// show login/panel button text dynamicly
add_shortcode('login_or_panel_button', 'login_or_panel_button_shortcode');
function login_or_panel_button_shortcode(){
  ob_start();
  echo is_user_logged_in() ? 'پنل کاربری':'ورود';
  return ob_get_clean();
}

